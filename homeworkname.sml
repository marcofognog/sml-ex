fun is_older (date1 : (int*int*int), date2 : (int*int*int)) =
  if #1 date1 < #1 date2 (* year *)
  then
      true
  else
      if #1 date1 = #1 date2
      then
          if #2 date1 < #2 date2 (* month *)
          then true
          else
              if #2 date1 = #2 date2
              then #3 date1 < #3 date2 (* day *)
              else false
      else
          false

fun sum_list (xs : int list)=
  if null xs
  then 0
  else hd xs + sum_list(tl xs)

fun number_in_month (ms : (int*int*int) list, month : int) =
  if null ms
  then 0
  else
      if #2(hd ms) = month
      then 1 + number_in_month(tl ms, month)
      else 0 + number_in_month(tl ms, month)

fun number_in_months (ms : (int*int*int) list, ns : int list) =
  if null ns
  then 0
  else number_in_month(ms, hd ns) + number_in_months(ms, tl ns)

fun dates_in_month (ms : (int*int*int) list, month : int)=
  if null ms
  then []
  else
      if #2(hd ms) = month
      then (hd ms)::dates_in_month(tl ms, month)
      else dates_in_month(tl ms, month)

fun dates_in_months (ms : (int*int*int) list, months : int list) =
  if null months
  then []
  else dates_in_month(ms, hd months) @ dates_in_months(ms, tl months)

fun get_nth (ss : string list, n : int) =
  if n = 1
  then hd ss
  else get_nth(tl ss, n - 1)

val month_list = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

fun date_to_string (year : int, month : int, day : int) =
  get_nth(month_list, month) ^ " " ^ Int.toString(day) ^ ", " ^ Int.toString(year)

fun invert (xs : int list) =
  if null xs
  then []
  else invert(tl xs) @ [(hd xs)]

fun number_before_reaching_sum (target : int, ls : int list) =
  let
      fun aux (count, acc, tgt, l) =
              if acc + hd(l) >= target
              then  count
              else aux(1 + count, hd(l) + acc, tgt, tl(l))
  in
      if null ls
      then 0
      else aux(0, 0, target, ls)
  end

val days = [31, 59, 90,120,151,181,212,243,273,304,334,365]

fun month_number_for (day : int, ds : int list, count : int) =
  if day <= hd(ds)
  then count
  else month_number_for(day, tl(ds), count + 1)

fun what_month (day : int) =
  month_number_for(day, days, 1)

fun month_range (day1 : int, day2 :int) =
  if day1 > day2
  then []
  else what_month(day1) :: month_range(day1 + 1, day2)

fun oldest_of (b : (int*int*int) list) =
  if length(b) = 2
  then
      if is_older(hd(b), hd(tl(b)))
      then [hd(b)]
      else [hd(tl(b))]
  else
      oldest_of(hd(b) :: oldest_of(tl(b)))

fun oldest (l : (int*int*int) list) =
  if null l
  then NONE
  else SOME(hd(oldest_of(l)))
