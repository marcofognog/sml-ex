(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several of the functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string(s1 : string, s2 : string) =
  s1 = s2

fun all_except_option (name, name_list) =
  case name_list of
      [] => NONE
    | x::xs => if same_string(x, name)
               then SOME(xs)
               else
                   case all_except_option(name, xs) of
                       NONE => NONE
                    |  SOME y => SOME(x::y)

fun get_substitutions1 (subs_list, name) =
  case subs_list of
      [] => []
    | x::xs => case all_except_option(name, x) of
                   NONE => []
                 | SOME y => y @ get_substitutions1(xs, name)

fun get_substitutions2 (subs_list, name) =
  let fun aux (ls, n, acc) =
      case ls of
          [] => acc
        | x::xs => case all_except_option(name, x) of
                           NONE => acc
                         | SOME ys => aux(xs, name, acc @ ys)
  in
      aux(subs_list, name, [])
  end


fun similar_names (name_list, full_name) =
  let fun similars_except_name (name_list, {first=fi, middle=mi, last=la}) =
        let
            val subs = get_substitutions1(name_list, fi);
            fun similar_n (substitutions, {first=fi, middle=mi, last=la}) =
              case substitutions of
                  [] => []
                | n::ns => {first=n, middle=mi, last=la} :: similar_n(ns, {first=fi,middle=mi, last=la})
        in case name_list of
               [] => []
             | n::ns => similar_n(subs, {first=fi, middle=mi, last=la}) @ similars_except_name(ns, {first=fi, middle=mi, last=la})
        end
  in
      full_name :: similars_except_name (name_list, full_name)
  end

(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw

exception IllegalMove

fun card_color (rank) =
  case rank of
      (Spades, _) => Black
    | (Clubs, _) => Black
    | (Diamonds, _) => Red
    | (Hearts, _) => Red

fun card_value (rank) =
  case rank of
      (_, Num n) => n
    | (_,Jack) => 10
    | (_,Queen) => 10
    | (_,King) => 10
    | (_,Ace) => 11

fun remove_card (cs, c, e) =
  case cs of
      [] => []
    | h::t => if h = c
              then t
              else
                  let val res = remove_card(t, c, e)
                  in if res = t
                     then raise e
                     else h::res
                  end

fun all_same_color (cards) =
  case cards of
      [] => true
    | [x] => true
    | x::(xs::xss) => card_color(x) = card_color(xs) andalso all_same_color(xs::xss)

fun sum_cards (cards) =
  let
      fun aux (acc,cardx) =
        case cardx of
            [] => acc
          | (c::cs) => aux(card_value(c)+acc, cs)
  in
      aux(0,cards)
  end

fun score (cards, goal) =
  let
      val total = sum_cards(cards);
      val preliminary = if total > goal
                        then (total - goal) * 3
                        else goal - total
  in
          if all_same_color(cards)
          then preliminary div 2
          else preliminary
  end

fun officiate (d, mvs, goal) =
  let fun aux(deck, moves, hand, preliminary) =
        case moves of
            [] => preliminary (*game over*)
          | m::ms => case m of
                         Discard c => let val new_hand = remove_card(hand,c,IllegalMove)
                                      in aux(deck, ms, new_hand, score(new_hand, goal)) end
                       | Draw => case deck of
                                     [] => preliminary (*game over*)
                                   | d::ds => let val new_hand = d::hand
                                              in
                                                  if sum_cards(new_hand) > goal
                                                  then score(new_hand, goal) (*game over*)
                                                  else aux(ds, ms, new_hand, score(new_hand, goal))
                                              end
  in
      aux(d, mvs, [], 0)
  end

val test1 = all_except_option ("string", ["string"]) = SOME []
val test12 = all_except_option ("string", ["string", "opa"]) = SOME ["opa"]

val test2 = get_substitutions1 ([["foo"],["there"]], "foo") = []
val test22 = get_substitutions1 ([["foo","faa"],["there"]], "foo") = ["faa"]

val test3 = get_substitutions2 ([["foo"],["there"]], "foo") = []
val test31 = get_substitutions2 ([["foo", "opa"],["there","foo"],["eba"]], "foo") = ["opa", "there"]

val test4 = similar_names ([["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]], {first="Fred", middle="W", last="Smith"}) =
            [{first="Fred", last="Smith", middle="W"}, {first="Fredrick", last="Smith", middle="W"},
             {first="Freddie", last="Smith", middle="W"}, {first="F", last="Smith", middle="W"}]

val test5 = card_color (Clubs, Num 2) = Black

val test6 = card_value (Clubs, Num 2) = 2

val test7 = remove_card ([(Hearts, Ace)], (Hearts, Ace), IllegalMove) = []

val test8 = all_same_color [(Hearts, Ace), (Hearts, Ace), (Diamonds, King), (Hearts, Queen)] = true

val test9 = sum_cards [(Clubs, Num 2),(Clubs, Num 2)] = 4

val test10 = score ([(Hearts, Num 2),(Clubs, Num 4)],10) = 4
val test101 = score ([(Hearts, Num 6),(Clubs, Num 6)],4) = 24
val test102 = score ([(Clubs, Num 2),(Clubs, Num 4)],10) = 2
val test103 = score([(Clubs,Ace),(Spades,Ace),(Clubs,Ace),(Spades,Ace)],42) = 3
val test104 = score([(Hearts, Num 4),(Clubs, Num 4)],4) = 12

(* Same color *)
val test11 = officiate ([(Hearts, Num 2),(Clubs, Num 4)],[Draw], 15) = 6
(* Two draws, differente color*)
val test112 = officiate ([(Hearts, Num 2),(Clubs, Num 4)],[Draw, Draw], 15) = 9
(* same color, game over by exceeding goal *)
val test113 = officiate ([(Hearts, Num 6),(Clubs, Num 4)],[Draw], 4) = 3
(* diff color, game over by empty deck *)
val test114 = officiate ([(Hearts, Num 4),(Clubs, Num 4)],[Draw, Draw, Draw], 4) = 12
(* Empty move list*)
val test115 = officiate ([(Hearts, Num 4),(Clubs, Num 4)],[], 4) = 0
(* Empty card list*)
val test116 = officiate ([],[Draw, Draw, Draw], 4) = 0

val test117= officiate ([(Clubs,Ace),(Spades,Ace),(Clubs,Ace),(Spades,Ace)],
                        [Draw,Draw,Draw,Draw,Draw],
                        42)
             = 3

val test13 = ((officiate([(Clubs,Jack),(Spades,Num(8))],
                         [Draw,Discard(Hearts,Jack)],
                         42);
               false)
              handle IllegalMove => true)

