(* Coursera Programming Languages, Homework 3, Provided Code *)

exception NoAnswer

datatype pattern = Wildcard
		 | Variable of string
		 | UnitP
		 | ConstP of int
		 | TupleP of pattern list
		 | ConstructorP of string * pattern

datatype valu = Const of int
	      | Unit
	      | Tuple of valu list
	      | Constructor of string * valu

fun g f1 f2 p =
    let
	val r = g f1 f2
    in
	case p of
	    Wildcard          => f1 ()
	  | Variable x        => f2 x
	  | TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
	  | ConstructorP(_,p) => r p
	  | _                 => 0
    end

fun count_wildcards (pat) =
  g (fn x => 1) (fn x => 0) pat

(**** for the challenge problem only ****)

datatype typ = Anything
	     | UnitT
	     | IntT
	     | TupleT of typ list
	     | Datatype of string

(**** you can put all your code here ****)

fun only_capitals (strs) =
  List.filter (fn x => Char.isUpper( String.sub(x, 0) )) strs

fun longest_string1 (strs) =
  List.foldl (fn (a, s) => if String.size(s) >= String.size(a) then s else a) "" strs

fun longest_string2 (strs) =
  List.foldl (fn (a, s) => if String.size(s) > String.size(a) then s else a) "" strs

fun longest_string_helper (f) =
  (fn l => List.foldl f "" l)

fun longest_string3 (strs) =
  longest_string_helper (fn (a,s) => if String.size(s) >= String.size(a) then s else a) strs

fun longest_string4 (strs) =
  longest_string_helper (fn (a,s) => if String.size(s) > String.size(a) then s else a) strs

fun longest_capitalized (strs) =
  let val lngststr = longest_string1 o only_capitals
  in
      lngststr(strs)
  end

fun rev_string (str) =
  (String.implode o List.rev o String.explode) (str)

fun first_answer (f) =
  let fun second (ls) =
        case ls of
            [] => raise NoAnswer
          | x::xs => case f(x) of
                         NONE => second(xs)
                       | SOME y => y
  in
      second
  end

fun all_answers (f) =
  let fun second (ls) =
        let fun aux (acc, lst) =
              case lst of
                  [] => SOME acc
                | x::xs => case f(x) of
                               SOME l => aux(acc @ l, xs)
                             | NONE => NONE
        in
            aux([],ls)
        end
  in
      second
  end

(* Homework3 Simple Test*)

val test1 = only_capitals ["A","B","C"] = ["A","B","C"]
val test1_2 = only_capitals ["a","B","C"] = ["B","C"]
val test1_3 = only_capitals ["Abacate","Banana","CENOURA"] = ["Abacate", "Banana","CENOURA"]

val test2 = longest_string1 ["A","bc","C"] = "bc"
val test2_2 = longest_string1 ["aa","bc","C"] = "aa"
val test2_3 = longest_string1 [] = ""

val test3 = longest_string2 ["A","bc","C"] = "bc"
val test3_2 = longest_string2 ["aa","bc","C"] = "bc"

val test4a = longest_string3 ["A","bc","C"] = "bc"
val test4a_2 = longest_string3 ["aa","bc","C"] = "aa"

val test4b = longest_string4 ["A","B","C"] = "C"
val test4b_2 = longest_string4 ["aa","bc","C"] = "bc"

val test5 = longest_capitalized ["A","bc","C"] = "A"
val test5_2 = longest_capitalized ["A","bc","Ciclovia"] = "Ciclovia"
val test5_3 = longest_capitalized ["a","bc","opa"] = ""

val test6 = rev_string "abc" = "cba"

val test7 = first_answer (fn x => if x > 3 then SOME x else NONE) [1,2,3,4,5] = 4
val test7_raise_ex = first_answer (fn x => if x > 6 then SOME x else NONE) [1,2,3,4,5] = 4

val test8 = all_answers (fn x => if x = 1 then SOME [x] else NONE) [2,3,4,5,6,7] = NONE
val test8_1 = all_answers (fn x => if x = 1 then SOME [x] else NONE) [1,1,1,1,1,1] = SOME [1,1,1,1,1,1]
val test8_2 = all_answers (fn x => if x = 1 then SOME [x] else NONE) [1,1,2,1] = NONE

val test9a1 = count_wildcards Wildcard = 1
val test9a2 = count_wildcards Wildcard = 1

val test9b = count_wild_and_variable_lengths (Variable("a")) = 1

val test9c = count_some_var ("x", Variable("x")) = 1

val test10 = check_pat (Variable("x")) = true

val test11 = match (Const(1), UnitP) = NONE

val test12 = first_match Unit [UnitP] = SOME []

